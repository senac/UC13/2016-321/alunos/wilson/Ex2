/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.ex2;

/**
 *
 * @author Administrador
 */
public class Fabrica {

    private double custoDeFabrica;
    private double custodoConsumidor;
    private double percentualdoDistribuidor;
    private double imposto;

    public Fabrica() {
    }

    public Fabrica(double custoDeFabrica, double percentualdoDistribuidor, double imposto) {
        this.custoDeFabrica = custoDeFabrica;
        this.percentualdoDistribuidor = percentualdoDistribuidor;
        this.imposto = imposto;
    }

    public double getCustoDeFabrica() {
        return custoDeFabrica;
    }

    public void setCustoDeFabrica(double custoDeFabrica) {
        this.custoDeFabrica = custoDeFabrica;
    }

    public double getCustodoConsumidor() {
        return custodoConsumidor;
    }

    public void setCustodoConsumidor(double custodoConsumidor) {
        this.custodoConsumidor = custodoConsumidor;
    }

    public double getPercentualdoDistribuidor() {
        return percentualdoDistribuidor;
    }

    public void setPercentualdoDistribuidor(double percentualdoDistribuidor) {
        this.percentualdoDistribuidor = percentualdoDistribuidor;
    }

    public double getImposto() {
        return imposto;
    }

    public void setImposto(double imposto) {
        this.imposto = imposto;
    }

}
