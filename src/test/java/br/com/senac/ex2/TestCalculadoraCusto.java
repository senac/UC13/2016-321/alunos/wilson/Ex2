/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.ex2;

import static junit.framework.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author Administrador
 */
public class TestCalculadoraCusto {

    @Test
    public void deveCalcularOcustoDoCarroAoConsumidor() {
        CalculadoraCusto custo = new CalculadoraCusto();
        Fabrica f = new Fabrica(15.000, 0.28, 0.45);

        double resultado = custo.calcular(f);
        assertEquals(25.950, resultado, 0.001);
    }
}
